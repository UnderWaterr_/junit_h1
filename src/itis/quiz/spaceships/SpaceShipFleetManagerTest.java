package itis.quiz.spaceships;

import java.util.ArrayList;

public class SpaceShipFleetManagerTest {
    SpaceshipFleetManager center;

    ArrayList<Spaceship> ships = new ArrayList<>();
    public static void main(String[] args) {
        SpaceShipFleetManagerTest spaceshipFleetManagerTest = new SpaceShipFleetManagerTest(new CommandCenter());

        int count = 0;
        boolean test1 = spaceshipFleetManagerTest.getMostPowerfulShip_ShipsNotExists_returnNull();

        if(test1){
            System.out.println("All good!");
            count++;
        }else{
            System.out.println("Test 1 failed!");
        }

        boolean test2 = spaceshipFleetManagerTest.getMostPowerfulShip_ShipsExists_returnTargetShip();


        if(test2){
            System.out.println("All good!");
            count++;

        }else{
            System.out.println("Test 2 failed!");
        }

        boolean test3 = spaceshipFleetManagerTest.getMostPowerfulShip_ShipsExists_returnFirstTargetShip();

        if(test3){
            System.out.println("All good!");
            count++;

        }else{
            System.out.println("Test 3 failed!");
        }

        boolean test4 = spaceshipFleetManagerTest.getShipByName_ShipsExists_returnTargetShip();

        if(test4){
            System.out.println("All good!");
            count++;

        }else{
            System.out.println("Test 4 failed!");
        }

        boolean test5 = spaceshipFleetManagerTest.getShipByName_ShipsNotExists_returnNull();

        if(test5){
            System.out.println("All good!");
            count++;

        }else{
            System.out.println("Test 5 failed!");
        }

        boolean test6 = spaceshipFleetManagerTest.getAllShipsWithEnoughCargoSpace_ShipsExists_returnTargetShip();

        if(test6){
            System.out.println("All good!");
            count++;

        }else{
            System.out.println("Test 6 failed!");
        }

        boolean test7 = spaceshipFleetManagerTest.getAllShipsWithEnoughCargoSpace_ShipsNotExists_returnNull();

        if(test7){
            System.out.println("All good!");
            count++;

        }else{
            System.out.println("Test 7 failed!");
        }


        boolean test8 = spaceshipFleetManagerTest.getAllCivilianShips_ShipsNotExists_returnNull();

        if(test8){
            System.out.println("All good!");
            count++;

        }else{
            System.out.println("Test 8 failed!");
        }

        boolean test9 = spaceshipFleetManagerTest.getAllCivilianShips_ShipsExists_returnTargetShips();

        if(test9){
            System.out.println("All good!");
            count++;

        }else{
            System.out.println("Test 9 failed!");
        }
        boolean test10 = spaceshipFleetManagerTest.isPowerful_AllShipsPowerful_returnTrue();

        if(test10){
            System.out.println("All good!");
            count++;

        }else{
            System.out.println("Test 9 failed!");
        }
        boolean test11 = spaceshipFleetManagerTest.isPowerful_NotAllShipsPowerful_returnFalse();

        if(test11){
            System.out.println("All good!");
            count++;

        }else{
            System.out.println("Test 9 failed!");
        }
        boolean test12 = spaceshipFleetManagerTest.countOfShipsWithoutNumber_NotAllShipsPowerful_returnThree();

        if(test12){
            System.out.println("All good!");
            count++;

        }else{
            System.out.println("Test 9 failed!");
        }
        boolean test13 = spaceshipFleetManagerTest.countOfShipsWithoutNumber_NotAllShipsPowerful_returnZero();

        if(test13){
            System.out.println("All good!");
            count++;

        }else{
            System.out.println("Test 9 failed!");
        }
        System.out.println(count);
    }

    //1корабля нет +++++
    public boolean getMostPowerfulShip_ShipsNotExists_returnNull(){
        ships.add(new Spaceship("1", 0, 15, 122));
        ships.add(new Spaceship("2", 0, 121, 126));
        ships.add(new Spaceship("3", 0, 14, 125));

        Spaceship mostPowerfulShip = center.getMostPowerfulShip(ships);

        if (mostPowerfulShip==null){
            ships.clear();
            return true;
        }

        ships.clear();
        return false;
    }

    //2вернул не самый вооружённый корабль +++++
    public boolean getMostPowerfulShip_ShipsExists_returnTargetShip(){
        int testPowerful = 30;

        ships.add(new Spaceship("1", 10, 15, 122));
        ships.add(new Spaceship("2", 5, 121, 126));
        ships.add(new Spaceship("3", testPowerful, 14, 125));
        Spaceship mostPowerfulShip = center.getMostPowerfulShip(ships);

        if (mostPowerfulShip.getFirePower()==testPowerful){
            ships.clear();
            return true;
        }

        ships.clear();
        return false;
    }

    //3вернул не 1 корабль ++++
    public boolean getMostPowerfulShip_ShipsExists_returnFirstTargetShip(){
        String mostPowerfulFirstShipName = "1";
        int mostPowerfulFirstShipFirePower = 100;

        ships.add(new Spaceship(mostPowerfulFirstShipName, mostPowerfulFirstShipFirePower, 15, 122));
        ships.add(new Spaceship("2", 52, 121, 126));
        ships.add(new Spaceship("3", mostPowerfulFirstShipFirePower, 14, 125));

        Spaceship mostPowerfulShip = center.getMostPowerfulShip(ships);

        if (mostPowerfulShip.getName().equals(mostPowerfulFirstShipName) && mostPowerfulShip.getFirePower()==mostPowerfulFirstShipFirePower){
            ships.clear();
            return true;
        }

        ships.clear();
        return false;
    }

    //4 не тот ко рабль
    public boolean  getShipByName_ShipsExists_returnTargetShip(){
        String shipName = "11";

        ships.add(new Spaceship(shipName, 10, 13, 12));
        ships.add(new Spaceship("2", 5, 14, 13));
        ships.add(new Spaceship("3", 30, 15, 15));

        Spaceship shipByName = center.getShipByName(ships, shipName);

        if (shipByName.getName().equals(shipName)){
            ships.clear();
            return true;
        }

        ships.clear();
        return false;
    }

    //5корабля нет
    public boolean  getShipByName_ShipsNotExists_returnNull(){
        String shipName = "11";

        ships.add(new Spaceship("1", 10, 13, 12));
        ships.add(new Spaceship("2", 5, 14, 13));
        ships.add(new Spaceship("3", 30, 15, 15));

        Spaceship shipByName = center.getShipByName(ships, shipName);

        if (shipByName==null){
            ships.clear();
            return true;
        }

        ships.clear();
        return false;
    }

    //6грузоподъёмность меньше нужной
    private boolean getAllShipsWithEnoughCargoSpace_ShipsExists_returnTargetShip(){
        int cargo = 20;
        ships.add(new Spaceship("1", 0, 25, 12));
        ships.add(new Spaceship("2", 5, 12, 12));
        ships.add(new Spaceship("3", 0, 30, 12));

        ArrayList<Spaceship> shipsWithEnoughCargoSpace = center.getAllShipsWithEnoughCargoSpace(ships, cargo);

        boolean flag = true;
        if (shipsWithEnoughCargoSpace.stream().anyMatch(spaceship -> spaceship.getCargoSpace() < cargo)){
            flag = false;
        }
        ships.clear();
        return flag;
    }

    //7корабля нет getAllShipsWithEnoughCargoSpace
    private boolean getAllShipsWithEnoughCargoSpace_ShipsNotExists_returnNull(){
        ships.add(new Spaceship("1", 104, 13, 12));
        ships.add(new Spaceship("2", 51, 14, 13));
        ships.add(new Spaceship("3", 304, 15, 15));

        ArrayList<Spaceship> shipsWithEnoughCargoSpace = center.getAllShipsWithEnoughCargoSpace(ships, 100);

        if (shipsWithEnoughCargoSpace.size()==0){
            ships.clear();
            return true;
        }

        ships.clear();
        return false;

    }

    //8корабля нет +
    private boolean getAllCivilianShips_ShipsNotExists_returnNull(){
        ships.add(new Spaceship("1", 10, 12, 12));
        ships.add(new Spaceship("2", 5, 12, 12));
        ships.add(new Spaceship("3", 30, 12, 12));

        ArrayList<Spaceship> allCivilianShips = center.getAllCivilianShips(ships);

        if (allCivilianShips.size()==0){
            ships.clear();
            return true;
        }

        ships.clear();
        return false;
    }

    //9вернул немирный корабль +
    private boolean getAllCivilianShips_ShipsExists_returnTargetShips(){
        ships.add(new Spaceship("1", 0, 12, 12));
        ships.add(new Spaceship("2", 5, 12, 12));
        ships.add(new Spaceship("3", 0, 12, 12));

        ArrayList<Spaceship> allCivilianShips = center.getAllCivilianShips(ships);
        boolean flag = true;
        if (allCivilianShips.stream().anyMatch(spaceship -> spaceship.getFirePower()!=0)){
            flag = false;
        }
        ships.clear();
        return flag;
    }

    //Проверки, все ли корабли из списка вооружены

    private boolean isPowerful_AllShipsPowerful_returnTrue() {
        ships.add(new Spaceship("1", 12, 12, 12));
        ships.add(new Spaceship("2", 5, 12, 12));
        ships.add(new Spaceship("3", 32, 12, 12));

        boolean result = center.isPowerful(ships);
        ships.clear();
        return result;
    }

    private boolean isPowerful_NotAllShipsPowerful_returnFalse() {
        ships.add(new Spaceship("1", 0, 12, 12));
        ships.add(new Spaceship("2", 5, 12, 12));
        ships.add(new Spaceship("3", 15, 12, 12));

        boolean result = center.isPowerful(ships);
        ships.clear();
        return !result;
    }

    //Подсчета количества кораблей имя которых не содержит цифр

    private boolean countOfShipsWithoutNumber_NotAllShipsPowerful_returnThree() {
        ships.add(new Spaceship("a", 0, 12, 12));
        ships.add(new Spaceship("b", 5, 12, 12));
        ships.add(new Spaceship("c", 15, 12, 12));

        long result = center.countOfShipsWithoutNumber(ships);
        ships.clear();
        return result == 3;
    }

    private boolean countOfShipsWithoutNumber_NotAllShipsPowerful_returnZero() {
        ships.add(new Spaceship("1", 0, 12, 12));
        ships.add(new Spaceship("2", 5, 12, 12));
        ships.add(new Spaceship("3", 15, 12, 12));

        long result = center.countOfShipsWithoutNumber(ships);
        ships.clear();
        return result == 0;
    }

    public SpaceShipFleetManagerTest(SpaceshipFleetManager center) {
        this.center = center;
    }

}