package itis.quiz.spaceships;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.stream.Collectors;


// Вы проектируете интеллектуальную систему управления ангаром командного центра.
// Реализуйте интерфейс SpaceshipFleetManager для управления флотом кораблей.
// Используйте СУЩЕСТВУЮЩИЙ интерфейс и класс космического корабля (SpaceshipFleetManager и Spaceship).
public class CommandCenter implements SpaceshipFleetManager {

    @Override
    public Spaceship getMostPowerfulShip(ArrayList<Spaceship> ships) {
        return ships.stream().max(Comparator.comparing(Spaceship::getFirePower)).filter(spaceship -> spaceship.getFirePower()>0).orElse(null);
    }

    @Override
    public Spaceship getShipByName(ArrayList<Spaceship> ships, String name) {
        return ships.stream().filter(spaceship -> spaceship.getName().equals(name)).findAny().orElse(null);
    }

    @Override
    public ArrayList<Spaceship> getAllShipsWithEnoughCargoSpace(ArrayList<Spaceship> ships, Integer cargoSize) {
        return  (ArrayList<Spaceship>) ships.stream().filter(spaceship -> spaceship.getCargoSpace() >= cargoSize).collect(Collectors.toList());
    }

    @Override
    public ArrayList<Spaceship> getAllCivilianShips(ArrayList<Spaceship> ships) {
        return  (ArrayList<Spaceship>) ships.stream().filter(spaceship -> spaceship.getFirePower() == 0).collect(Collectors.toList());
    }

    @Override
    public  boolean isPowerful(ArrayList<Spaceship> ships){
        return ships.stream().allMatch(spaceship -> spaceship.getFirePower() > 0);
    }

    @Override
    public long countOfShipsWithoutNumber(ArrayList<Spaceship> ships){
        return ships.stream().filter(spaceship -> !spaceship.getName().contains("0") && !spaceship.getName().contains("1")
                && !spaceship.getName().contains("2") && !spaceship.getName().contains("3") && !spaceship.getName().contains("4")
                && !spaceship.getName().contains("5") && !spaceship.getName().contains("6") && !spaceship.getName().contains("7")
                && !spaceship.getName().contains("8") && !spaceship.getName().contains("9")).count();
    }
}
