package itis.quiz.spaceships;

import org.junit.jupiter.api.*;

import java.util.ArrayList;

public class SpaceShipFleetManagerTestJunit {
    SpaceshipFleetManager center = new CommandCenter();
    ArrayList<Spaceship> ships = new ArrayList<>();

    @BeforeEach
    void addShips() {
        ships.add(new Spaceship("1", 10, 15, 122));
        ships.add(new Spaceship("2", 5, 121, 126));
        ships.add(new Spaceship("3", 15, 14, 125));
    }

    @AfterEach
    void clearAllShips() {
        ships.clear();
    }

    @Test
    @DisplayName("вернул не самый вооружённый корабль")
    public void getMostPowerfulShip_ShipsExists_returnTargetShip() {
        int testPowerful = 30;
        ships.add(new Spaceship("4", testPowerful, 15, 122));

        Spaceship mostPowerfulShip = center.getMostPowerfulShip(ships);

        Assertions.assertEquals(mostPowerfulShip.getFirePower(), testPowerful);
    }

    @Test
    @DisplayName("самого вооружённого корабля нет")
    public void getMostPowerfulShip_ShipsNotExists_returnNull() {
        ships.clear();
        ships.add(new Spaceship("1", 0, 15, 122));
        ships.add(new Spaceship("2", 0, 121, 126));
        ships.add(new Spaceship("3", 0, 14, 125));

        Assertions.assertNull(center.getMostPowerfulShip(ships));
    }

    @Test
    @DisplayName("вернул не 1 корабль вооружённый корабль")
    public void getMostPowerfulShip_ShipsExists_returnFirstTargetShip() {
        String mostPowerfulFirstShipName = "1";
        int mostPowerfulFirstShipFirePower = 100;
        ships.add(new Spaceship(mostPowerfulFirstShipName, mostPowerfulFirstShipFirePower, 15, 122));
        Spaceship mostPowerfulShip = center.getMostPowerfulShip(ships);

        Assertions.assertEquals(mostPowerfulShip.getName(), mostPowerfulFirstShipName);
        Assertions.assertEquals(mostPowerfulShip.getFirePower(), mostPowerfulFirstShipFirePower);
    }

    @Test
    @DisplayName("вернул корабль не с тем именем")
    public void getShipByName_ShipsExists_returnTargetShip() {
        String shipName = "11";
        ships.add(new Spaceship(shipName, 10, 13, 12));

        Assertions.assertEquals(center.getShipByName(ships, shipName).getName(), shipName);
    }

    @Test
    @DisplayName("нет корабля с нужным именем")
    public void getShipByName_ShipsNotExists_returnNull() {
        String shipName = "11";

        Assertions.assertNull(center.getShipByName(ships, shipName));
    }

    @Test
    @DisplayName("грузоподъёмность корабля меньше нужной")
    public void getAllShipsWithEnoughCargoSpace_ShipsExists_returnTargetShip() {
        int cargo = 20;

        ArrayList<Spaceship> shipsWithEnoughCargoSpace = center.getAllShipsWithEnoughCargoSpace(ships, 100);

        boolean flag = true;
        if (shipsWithEnoughCargoSpace.stream().anyMatch(spaceship -> spaceship.getCargoSpace() < cargo)){
            flag = false;
        }
        Assertions.assertTrue(flag);
    }

    @Test
    @DisplayName("корабля с нужной грузоподъёмностью нет")
    public void getAllShipsWithEnoughCargoSpace_ShipsNotExists_returnNull() {
        Assertions.assertEquals(center.getAllShipsWithEnoughCargoSpace(ships, 125).size(), 0);

    }

    @Test
    @DisplayName("мирного корабля нет")
    public void getAllCivilianShips_ShipsNotExists_returnNull() {
        Assertions.assertEquals(center.getAllCivilianShips(ships).size(), 0);
    }

    @Test
    @DisplayName("вернул немирный корабль ")
    public void getAllCivilianShips_ShipsExists_returnTargetShips() {
        ships.add(new Spaceship("4", 0, 12, 12));

        ArrayList<Spaceship> allCivilianShips = center.getAllCivilianShips(ships);
        boolean flag = true;
        if (allCivilianShips.stream().anyMatch(spaceship -> spaceship.getFirePower()!=0)){
            flag = false;
        }
        Assertions.assertTrue(flag);
    }


    //Проверки, все ли корабли из списка вооружены
    @Test
    @DisplayName("все корабли военные")
    public void isPowerful_AllShipsPowerful_returnTrue() {
        Assertions.assertTrue(center.isPowerful(ships));
    }

    @Test
    @DisplayName("есть невоенный корабль ")
    public void isPowerful_NotAllShipsPowerful_returnFalse() {
        ships.add(new Spaceship("4", 0, 12, 12));
        Assertions.assertFalse(center.isPowerful(ships));
        ships.remove(ships.size() - 1);
    }

    //Подсчета количества кораблей имя которых не содержит цифр


    @Test
    @DisplayName("все корабли с цифрами в имени ")
    public void countOfShipsWithoutNumber_NotAllShipsPowerful_returnZero() {
        Assertions.assertEquals(center.countOfShipsWithoutNumber(ships), 0);
    }

    @Test
    @DisplayName("3 корабля без цифр в имени ")
    public void countOfShipsWithoutNumber_NotAllShipsPowerful_returnThree() {
        ships.add(new Spaceship("a", 0, 12, 12));
        ships.add(new Spaceship("b", 5, 12, 12));
        ships.add(new Spaceship("c", 15, 12, 12));

        Assertions.assertEquals(center.countOfShipsWithoutNumber(ships), 3);
    }
}
